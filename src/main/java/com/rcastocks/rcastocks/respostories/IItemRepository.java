package com.rcastocks.rcastocks.respostories;

import com.rcastocks.rcastocks.beans.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IItemRepository extends JpaRepository<Item,Long> {
}
