package com.rcastocks.rcastocks.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class SoapWebServiceConf  {
    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<MessageDispatcherServlet>(messageDispatcherServlet, "/WS/daniel/stocks/*");
    }



    @Bean
    public XsdSchema itemSchema() {
        return new SimpleXsdSchema(new ClassPathResource("items.xsd"));
    }

    @Bean(name = "suppliers")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema suppliersSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("SupplierPort");
        definition.setTargetNamespace("http://danieldukundane.com/suppliers");
        definition.setLocationUri("/WS/daniel/stocks");
        definition.setSchema(suppliersSchema());
        return definition;
    }

    @Bean
    public XsdSchema suppliersSchema() {
        return new SimpleXsdSchema(new ClassPathResource("suppliers.xsd"));
    }

    @Bean(name = "items")
    public DefaultWsdl11Definition defaultWsdl11DefinitionItem(XsdSchema itemSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("ItemsPort");
        definition.setTargetNamespace("http://danieldukundane.com/items");
        definition.setLocationUri("/WS/daniel/stocks");
        definition.setSchema(itemSchema);
        return definition;
    }
}