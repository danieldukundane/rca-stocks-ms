package com.rcastocks.rcastocks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RcastocksApplication {

    public static void main(String[] args) {
        SpringApplication.run(RcastocksApplication.class, args);
    }

}
