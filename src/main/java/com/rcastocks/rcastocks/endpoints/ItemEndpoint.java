package com.rcastocks.rcastocks.endpoints;


import com.danieldukundane.items.*;
import com.rcastocks.rcastocks.beans.Item;
import com.rcastocks.rcastocks.beans.Supplier;
import com.rcastocks.rcastocks.respostories.IItemRepository;
import com.rcastocks.rcastocks.respostories.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
public class ItemEndpoint {

    @Autowired
    private IItemRepository itemRepository;

    @Autowired
    private SupplierRepository supplierRepository;



    @PayloadRoot(namespace = "http://danieldukundane.com/items", localPart = "saveItemRequest")
    @ResponsePayload
    public SaveItemResponse saveItemResponse(@RequestPayload SaveItemRequest request){

        Supplier supplierFound = supplierRepository.findById(request.getSupplierId()).get();

        Item item = new Item(request.getName(), request.getItemCode(), request.getStatus(), request.getPrice(), supplierFound);

        item = itemRepository.save(item);

        SaveItemResponse response = new SaveItemResponse();

        com.danieldukundane.items.Item itemSaved = new com.danieldukundane.items.Item();
        itemSaved.setId(item.getId());
        itemSaved.setItemCode(item.getItemCode());
        itemSaved.setName(item.getName());
        itemSaved.setPrice(item.getPrice());
        itemSaved.setStatus(item.getStatus());
        itemSaved.setSupplierId(item.getSupplier().getSupplierId());

        response.setItem(itemSaved);

        return  response;
    }

    @PayloadRoot(namespace = "http://danieldukundane.com/items", localPart = "getAllItemsRequest")
    @ResponsePayload
    public GetAllItemsResponse getAll(){

        GetAllItemsResponse response = new GetAllItemsResponse();

        List<Item> items = itemRepository.findAll();

        for (Item item : items ){
            response.getItem().add(mapItemBean(item));
        }

        return  response;
    }

    @PayloadRoot(namespace = "http://danieldukundane.com/items", localPart = "getItemRequest")
    @ResponsePayload
    public GetItemResponse getById(@RequestPayload GetItemRequest request){

        GetItemResponse response = new GetItemResponse();

        Item itemFound = itemRepository.findById(request.getItemId()).get();

        response.setItem(mapItemBean(itemFound));

        return  response;
    }

    @PayloadRoot(namespace = "http://danieldukundane.com/items", localPart = "updateItemResponse")
    @ResponsePayload
    public UpdateItemResponse updateById(@RequestPayload UpdateItemRequest request){

        UpdateItemResponse response = new UpdateItemResponse();

        Supplier supplierFound = supplierRepository.findById(request.getSupplierId()).get();

        Item itemToUpdate = new Item(request.getName(), request.getItemCode(), request.getStatus(), request.getPrice(), supplierFound);
        itemToUpdate.setId(request.getId());

        itemRepository.save(itemToUpdate);

        response.setItem(mapItemBean(itemToUpdate));

        return  response;
    }

    @PayloadRoot(namespace = "http://danieldukundane.com/items", localPart = "deleteItemRequest")
    @ResponsePayload
    public DeleteItemResponse deleteItem(@RequestPayload DeleteItemRequest request){

        DeleteItemResponse response = new DeleteItemResponse();

        Item itemFound = itemRepository.findById(request.getId()).get();

        response.setItem(mapItemBean(itemFound));

        itemRepository.deleteById(request.getId());

        return  response;
    }




    public com.danieldukundane.items.Item mapItemBean(Item item){
        com.danieldukundane.items.Item itemFound = new com.danieldukundane.items.Item();
        itemFound.setId(item.getId());
        itemFound.setItemCode(item.getItemCode());
        itemFound.setName(item.getName());
        itemFound.setPrice(item.getPrice());
        itemFound.setStatus(item.getStatus());
        itemFound.setSupplierId(item.getSupplier().getSupplierId());

        return itemFound;
    }

    public Item mapItem(com.danieldukundane.items.Item item){
        Item itemFound = new Item();
        itemFound.setId(item.getId());
        itemFound.setItemCode(item.getItemCode());
        itemFound.setName(item.getName());
        itemFound.setPrice(item.getPrice());
        itemFound.setStatus(item.getStatus());

        Supplier supplier = supplierRepository.findById(item.getSupplierId()).get();

        itemFound.setSupplier(supplier);

        return itemFound;
    }



}

