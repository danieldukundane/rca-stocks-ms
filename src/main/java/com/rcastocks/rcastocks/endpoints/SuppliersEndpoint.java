package com.rcastocks.rcastocks.endpoints;

import com.danieldukundane.suppliers.*;
import com.rcastocks.rcastocks.respostories.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class SuppliersEndpoint {
    private final SupplierRepository supplierRepository;

    @Autowired
    public SuppliersEndpoint(SupplierRepository repository) {
        this.supplierRepository = repository;
    }

    @PayloadRoot(namespace = "http://danieldukundane.com/suppliers", localPart = "NewSupplierRequest")
    @ResponsePayload
    public NewSupplierResponse create(@RequestPayload NewSupplierRequest dto) {


        Supplier __supplier = dto.getSupplier();

        com.rcastocks.rcastocks.beans.Supplier _supplier  = new com.rcastocks.rcastocks.beans.Supplier(__supplier.getNames(),__supplier.getEmail(),__supplier.getMobile());
        _supplier.setSupplierId((long) __supplier.getId());


        com.rcastocks.rcastocks.beans.Supplier supplier = supplierRepository.save(_supplier);

        NewSupplierResponse response = new NewSupplierResponse();

        __supplier.setId(supplier.getSupplierId());

        response.setSupplier(__supplier);

        return response;
    }

    @PayloadRoot(namespace = "http://danieldukundane.com/suppliers", localPart = "GetAllSuppliersRequest")
    @ResponsePayload
    public GetAllSuppliersResponse findAll(@RequestPayload GetAllSuppliersRequest request){

        List<com.rcastocks.rcastocks.beans.Supplier> suppliers = supplierRepository.findAll();

        GetAllSuppliersResponse response = new GetAllSuppliersResponse();

        for (com.rcastocks.rcastocks.beans.Supplier supplier: suppliers){
            response.getSupplier().add(mapUserEntity(supplier));
        }

        return response;
    }
//
    @PayloadRoot(namespace = "http://danieldukundane.com/suppliers", localPart = "GetSupplierDetailsRequest")
    @ResponsePayload
    public GetSupplierDetailsResponse findById(@RequestPayload GetSupplierDetailsRequest request){
        Optional<com.rcastocks.rcastocks.beans.Supplier> _supplier = supplierRepository.findById(request.getId());

        if(!_supplier.isPresent())
            return new GetSupplierDetailsResponse();

        com.rcastocks.rcastocks.beans.Supplier supplier = _supplier.get();

        GetSupplierDetailsResponse response = new GetSupplierDetailsResponse();

        Supplier __supplier = mapUserEntity(supplier);

        response.setSupplier(__supplier);

        return response;
    }
//
    @PayloadRoot(namespace = "http://danieldukundane.com/suppliers", localPart = "DeleteSupplierRequest")
    @ResponsePayload
    public DeleteSupplierResponse delete(@RequestPayload DeleteSupplierRequest request){
        supplierRepository.deleteById(request.getId());
        DeleteSupplierResponse response = new DeleteSupplierResponse();
        response.setMessage("Successfully deleted a message");
        return response;
    }
//
    @PayloadRoot(namespace = "http://danieldukundane.com/suppliers", localPart = "UpdateSupplierRequest")
    @ResponsePayload
    public UpdateSupplierResponse update(@RequestPayload UpdateSupplierRequest request){
        Supplier __supplier = request.getSupplier();

        com.rcastocks.rcastocks.beans.Supplier _supplier  = new com.rcastocks.rcastocks.beans.Supplier(__supplier.getNames(),__supplier.getEmail(),__supplier.getMobile());
        _supplier.setSupplierId((long) __supplier.getId());

        com.rcastocks.rcastocks.beans.Supplier supplier = supplierRepository.save(_supplier);

        UpdateSupplierResponse supplierDTO = new UpdateSupplierResponse();

        __supplier.setId(supplier.getSupplierId());

        supplierDTO.setSupplier(__supplier);

        return supplierDTO;
    }

    private Supplier mapUserEntity(com.rcastocks.rcastocks.beans.Supplier supplier) {
        Supplier supplierFound = new Supplier();
        supplierFound.setId(supplier.getSupplierId());
        supplierFound.setEmail(supplier.getEmail());
        supplierFound.setMobile(supplier.getMobile());
        supplierFound.setNames(supplier.getNames());

        return supplierFound;
    }
}
